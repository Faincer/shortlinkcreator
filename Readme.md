
## Local setup
### Abststract:
The main task of the project: generation of a short link and its further use, instead of the original long link.
The short link is stored in the Redis cache.
### Preparing for start:
- Install docker
- Build project:
```bash
docker-compose -f Docker/docker-compose.yml build 
```
### Start project:
```bash
docker-compose -f Docker/docker-compose.yml up
# with daemon
docker-compose -f Docker/docker-compose.yml up -d
```
Open your browser and check the page http://127.0.0.1
### Test:
```bash
docker-compose -f Docker/docker-compose.yml run app /bin/bash -c "python manage.py test"
# with coverage
docker-compose -f Docker/docker-compose.yml run app /bin/bash -c "coverage run --source='.' manage.py test"
# show report
docker-compose -f Docker/docker-compose.yml run app /bin/bash -c "coverage report"
```