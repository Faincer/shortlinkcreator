import os
from django.shortcuts import render, redirect
from django.views.generic import CreateView, DetailView
from django.http import HttpResponseNotFound
from django.core.cache import cache
from .generate_shortlink import valid_url, generate_shortlink


class ShortLinkCreateView(CreateView):
    template_name = 'create_shortlink.html'

    def get(self, request, *args, **kwargs):
        context = {}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        post = request.POST
        original_link = post.get('longlink')
        if valid_url(original_link):
            short_link = generate_shortlink(cache)
            cache.set(short_link, original_link)
            answer = (f'http://{os.getenv("ALLOWED_HOST")}:8000/'
                      f're/{short_link}')
        else:
            answer = 'Error: invalid input data'
        context = {'longlink': original_link,
                   'shortlink': answer}
        return render(request, self.template_name, context)


class ShortLinkDetailView(DetailView):
    slug_url_kwarg = 'shortlink'

    def get(self, request, *args, **kwargs):
        short_link = kwargs.get('shortlink')
        try:
            original_link = cache.get(short_link)
            response = redirect(original_link)
        except Exception:
            response = HttpResponseNotFound('<h1>Page not found</h1>')
        return response
