import random
import string
import re


def valid_url(url: str) -> bool:
    ''' Check original link on valid '''
    regex = re.compile(
        # http:// or https://
        r'^(?:http|ftp)s?://'
        # domain...
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)'
        r'+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'
        # localhost...
        r'localhost|'
        # ...or ip
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'
        # optional port
        r'(?::\d+)?'
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return re.match(regex, url) is not None


def get_shortlink(stringLength: int = 5) -> str:
    '''
    Get random string with a specific length
    and characters([A..Z],[a..z], [0-9])
    '''
    letters = string.ascii_letters
    numbers = '0123456789'
    symbols = list(letters+numbers)
    random.shuffle(symbols)
    return ''.join(random.choice(symbols) for i in range(stringLength))


def generate_shortlink(cache) -> str:
    '''
    Generate shortlink that not in cache
    '''
    while 1:
        shortlink = get_shortlink()
        if shortlink not in cache:
            break
    return shortlink
