
from django.test import SimpleTestCase, Client
from django.urls import reverse


class TestShortLinkView(SimpleTestCase):
    client = Client()

    def test_shortlink_rendering_main_page(self):
        """
        Check rendering main page
        """
        url = reverse('create')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 200)

    def test_shortlink_creating_valid_data(self):
        """
        Check creating short link with valid data.
        Must be return 200 and new link.
        """
        url = reverse('create')
        data = {'longlink': 'https://yandex.ru'}

        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['shortlink'].startswith('htt'), True)

    def test_shortlink_creating_invalid_data(self):
        """
        Check creating short link with invalid data.
        Must be return error
        """
        url = reverse('create')
        data = {'longlink': 'foo.bar/dar'}

        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['shortlink'].startswith('Err'), True)

    def test_shortlink_check_redirect_true(self):
        """
        Create short link. Try to redirect with this link
        Must be return 302
        """
        url = reverse('create')
        data = {'longlink': 'https://www.google.ru'}

        response = self.client.post(url, data=data, format='json')
        short_link = response.context['shortlink']

        response = self.client.get(short_link, format='json')
        self.assertEqual(response.status_code, 302)

    def test_shortlink_check_redirect_false(self):
        """
        Try to redirect with wrong link from this site.
        Must be return 404
        """
        url = reverse('redirect', kwargs={'shortlink': 'google1234'})

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, 404)
